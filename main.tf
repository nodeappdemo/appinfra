# sets the provide as azure 
provider "azurerm" {
}

#Creates Azure resource group

resource "azurerm_resource_group" "webapp" {
  name     = "${var.envPrefix}RG"
  location = "${var.location}"
}

#Creates Service plan


module "webserviceplan" {
  source              = "./modules/webserviceplan"
  envPrefix           = var.envPrefix
  location            = "${azurerm_resource_group.webapp.location}"
  resource_group_name = "${azurerm_resource_group.webapp.name}"
  os_kind             = var.os_kind
  sp_size             = var.sp_size
  sp_capacity         = var.sp_capacity
  sp_tier             = var.sp_tier
}

#Creating App service

module "webapp" {
  source              = "./modules/webapp"
  sitename            = var.sitename
  envPrefix           = var.envPrefix
  location            = "${azurerm_resource_group.webapp.location}"
  resource_group_name = "${azurerm_resource_group.webapp.name}"
  service_plan_id     = module.webserviceplan.id
  webcontainer        = var.webcontainer
  webport             = var.webport
}
