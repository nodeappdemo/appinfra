resource "azurerm_app_service_plan" "webserviceplan" {
  name                = "${var.envPrefix}serviceplan"
  location            = var.location
  resource_group_name = var.resource_group_name
  reserved            = true
  
  kind                = var.os_kind
  
  sku {
    tier     = var.sp_tier
    size     = var.sp_size
    capacity = var.sp_capacity
  }
}