variable "envPrefix" {
  description = "environment"
}

variable "location" {
  description = "location"
}

variable "os_kind" {
  description = "Linux or Windows"
}

variable "sp_size" {
  description = "Size"
}

variable "sp_capacity" {
  description = "Size"
}

variable "sp_tier" {
  description = "Tier"
}
variable "resource_group_name" {
  description = "resource group name"
}