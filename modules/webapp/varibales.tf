variable "sitename" {
  description = "my site"
  default = "networkdemowebservice"
}

variable "envPrefix" {
  description = "environment"
}

variable "location" {
  description = "location"
}


variable "webport" {
  description = "port"
}

variable "webcontainer" {
  description = "container name"
}

variable "resource_group_name" {
  description = "resource group name"
}

variable "service_plan_id" {
  description = "app service plan id"
}
