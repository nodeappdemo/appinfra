resource "azurerm_app_service" "webapp" {
  name                = var.sitename
  location            = var.location
  resource_group_name = var.resource_group_name
  app_service_plan_id = var.service_plan_id

  site_config {
    linux_fx_version          = "DOCKER|${var.webcontainer}"
    use_32_bit_worker_process = true
  }
  app_settings = {
    "WEBSITES_PORT" = "${var.webport}"
  }
}