variable "sitename" {
  description = "my site"
  default     = "networkdemowebservice"
}

variable "envPrefix" {
  description = "environment"
  default     = "demo"
}

variable "location" {
  description = "location"
  default     = "centralus"
}


variable "webport" {
  description = "port"
  default     = "5000"
}

variable "webcontainer" {
  description = "container name"
  default     = "tzmozumder/mydemonode:latest"
}


variable "plan_settings" {
  type        = "map"
  description = "Definition of the dedicated plan to use"

  default = {
    kind     = "Linux" # Linux or Windows
    size     = "F1"
    capacity = 1
    tier     = "Free"
  }
}

variable "os_kind" {
  description = "Linux or Windows"
  default     = "Linux"
}

variable "sp_size" {
  description = "Size"
  default     = "F1"
}

variable "sp_capacity" {
  description = "Size"
  default     = "1"
}

variable "sp_tier" {
  description = "Tier"
  default     = "Free"
}

